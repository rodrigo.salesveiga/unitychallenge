﻿using UnityEngine;
using UnityEngine.Events;

public class RotateKey : MonoBehaviour
{
    public float MaxRotation, MinRotation;
    public UnityEvent OnHitMax, OnHitMin;
}
