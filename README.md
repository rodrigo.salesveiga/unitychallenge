# Desafio Unity
Um objeto chamado Key está posicionado na cena, com rotações e posição randômica, que não podem ser alteradas. O objetivo do desafio é triggar 2 eventos, OnHitMin, OnHitMax, uma única vez, sempre ao chegar na rotação mínima e máxima estipuladas nas variáveis MaxRotation e MinRotation, calculadas a partir da rotação original já estabelecida.

Nós queremos testar rotacionando o objeto pelo próprio gizmo, rotacionando apenas no eixo X local, em play mode.

Em resumo:

```js
public UnityEvent OnHitMax; // Deve ser triggado uma única vez, sempre que a diferença da rotação atual com a original for >= MaxRotation

public UnityEvent OnHitMin; // Deve ser triggado uma única vez, sempre que a diferença da rotação atual com a original for <= MinRotation
````
## Entrega
Para entregar o objetivo, envie um .rar da pasta do projeto com a Feature implementada, para rodrigo.salesveiga@gmail.com
